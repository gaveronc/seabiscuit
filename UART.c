#include "UART.h"

int Serial_Open(void) {
	RCC_APB2ENR |= 0x4;//Enable clock to bus
	RCC_APB1ENR |= 1 << 17;//Enable USART2 bank
	GPIOA_CRL &= ~0xFF00;//Mask bits
	GPIOA_CRL |= 0x0B00;//Set TX pin to AFIO push-pull (0xB)
	GPIOA_CRL |= 0x8000;//Set RX pin to input floating (0x4)
	USART2_BRR = 0x138;//Should set bps to 115kHz
	USART2_CR1 |= 1 << 13 |
	              1 << 3 |
	              1 << 2;//Enable bits
	
	return 1;//Assume success for the moment
}

int Serial_Close(void) {
	RCC_APB1ENR &= ~(1 << 17);//Disable USART2 bank
	GPIOA_CRL &= ~0xFF00;//Mask bits
	GPIOA_CRL |= 0x4400;//Reset GPIO pins being used
	USART2_CR1 = 0;//Reset control register
	return 0;
}

uint8_t GetByte(void) {
	//This function assumes a transmission will arrive and will wait forever for it
	while(!(USART2_SR & (1 << 5)));
	return USART2_DR;
}

//Returns true if something has been received over UART
uint8_t ByteReady(void) {
	return USART2_SR & (1 << 5);
}

void SendByte(uint8_t B) {
	USART2_DR = B;
	while(!(USART2_SR & (1 << 6)));//Wait for transmission to complete
}

//Send a line one character at a time
void SendLine(char * str) {
	int i;
	for (i = 0; str[i] != 0x0; i += 1) {
		SendByte(str[i]);
	}
}

//Turn a number into ascii characters, then send it
//Calls itself recursively to accomplish this
int SendInt(int num) {
	if (num < 0) {//Send negative sign if negative, then make positive
		SendByte('-');
		num = -num;
	}
	if (num > 9) {
		SendInt(num/10);
	}
	//Send ASCII character for given number
	SendByte((num % 10) + 48);
	return 1;
}
