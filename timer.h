#ifndef TIMER_H
#define TIMER_H
#include "registers.h"
#include "control.h"
#include <stdint.h>

void InitPWMTimer(void);
void PWMTimerStart(void);
void PWMTimerStop(void);
void PWMAngleSet(uint16_t percent);

#endif
