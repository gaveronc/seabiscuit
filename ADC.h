#ifndef ADC_H
#define ADC_H
#include "registers.h"
#include "control.h"
#include <stdint.h>

void ADCInit(void);
void EnableADCInt(void);
void DisableADCInt(void);
void StartADC(void);
uint16_t GetADC(void);
void ADC1_2_IRQHandler(void);

#endif
