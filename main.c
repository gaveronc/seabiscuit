/*
* Cameron Gaveronski
* 200230075
* ENEL 487
* Lab 3
*/

#include <stdint.h>
#include "registers.h"
#include "UART.h" //Enable UART functions
#include "commands.h"
#include "timer.h"
#include "ADC.h"
#include "control.h"
#include "SeaBiscuit.h"

/*
* This program implements a command prompt on the lab board
* that can be accessed over a serial COM port using putty
* or another similar serial terminal program.
* This program maps character arrays to function calls by
* taking all of the input up to a certain point and comparing
* it to an array of strings to try finding a match. When a
* complete match is found, everything following the function
* call is treated as the function's argument. To do this in a
* scalable way, each function, whether it uses arguments or
* not, has to take a pointer to a character array.
* 
* More important than the parsing methods, however, is the
* newly-implemented valve control features. First, the valve
* controls must be turned on with VALVE ON. Then the angle of
* the valve can be set with SET ANGLE <percentage>.
* 
*/

//Initialize clocks and GPIO pins
void SysInit(void) {
	//Need to setup the the LED's on the board
	RCC_APB2ENR |= 	1 << 3 |	// Enable Port B clock
									1 << 4 | 	// Enable Port C clock
									1 << 9;		// Enable ADC1 clock
	GPIOB_ODR  &= ~0x0000FF00; // Switch off LEDs
	GPIOB_CRL  = 0xB3333333;
	GPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
}

int main(void)
{
	uint8_t commandByte;
	struct command_struct config;
	
	//Initialize clocks and GPIO pins
	SysInit();
	
	//Set up timers
	InitPWMTimer();
	
	//Initialize ADC
	ADCInit();
	
	Serial_Open();//Start serial communications
	
	config.sea_name = "SeaBiscuit";
	config.sea_command_length = COMMAND_LENGTH;
	config.sea_command_num = COMMAND_LIST_LENGTH + 2;
	config.sea_command_history_size = COMMAND_BUFFER_LENGTH;
	config.sea_echo_cmd = SendByte;
	
	config.sea_command_list = malloc((COMMAND_LIST_LENGTH + 2) * sizeof(char *));
	config.sea_function_calls = malloc((COMMAND_LIST_LENGTH + 2) * sizeof(void *));
	
	config.sea_command_list[0] = "LED ON ";
	config.sea_function_calls[0] = LED_ON;
	config.sea_command_list[1] = "LED OFF ";
	config.sea_function_calls[1] = LED_OFF;
	config.sea_command_list[2] = "HELP";
	config.sea_function_calls[2] = HELP;
	config.sea_command_list[3] = "QUERY LED ";
	config.sea_function_calls[3] = QUERY_LED;
	config.sea_command_list[4] = "INFO";
	config.sea_function_calls[4] = INFO;
	config.sea_command_list[5] = "SET ANGLE ";
	config.sea_function_calls[5] = SETANGLE;
	config.sea_command_list[6] = "VALVE ON ";
	config.sea_function_calls[6] = VALVEON;
	config.sea_command_list[7] = "VALVE OFF ";
	config.sea_function_calls[7] = VALVEOFF;
	config.sea_command_list[8] = "ADC READ";
	config.sea_function_calls[8] = ADCREAD;
	config.sea_command_list[9] = "CONTROL ON ";
	config.sea_function_calls[9] = CONTROL_ON;
	config.sea_command_list[10] = "CONTROL OFF ";
	config.sea_function_calls[10] = CONTROL_OFF;
	config.sea_command_list[11] = "HISTORY";
	config.sea_function_calls[11] = sea_history_cmd;
	config.sea_command_list[12] = "!";
	config.sea_function_calls[12] = sea_bang_function;
	
	//SendLine(" \n\rSeaBiscuit > ");//Command prompt
	InitBiscuit(&config);
	
	while(1) {
		if(ADC_FLAG || ByteReady()) {
			if (!ByteReady()) {
				//Handle control system functions
				PControl();
				//Start another conversion
				StartADC();
			}	else {
				//Command byte is ready
				commandByte = GetByte();
				ParseByte(commandByte);
			}
		}
	}
}
