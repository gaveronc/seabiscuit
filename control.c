#include "control.h"

uint16_t ADCvCM[6] = {
	3366,
	3128,
	2589,
	2159,
	1552,
	1170
};

uint8_t ADC_FLAG = 0;//Conversion complete
uint16_t ADC_VAL = 0;//Communication with main
uint16_t SETPOINT = 0;//Valve set point, currently not used
uint16_t CONTROL_GAIN = 1;//Control loop proportional gain

//Convert cm to ADC value range
uint16_t Y (uint16_t dist) {
	if (dist > 3000) {
		return 1100;
	} else {
		return M * (((float)dist/100) - 5) + B;
	}
}

//Convert ADC to cm with 2 decimal places
uint16_t X (uint16_t adc) {
	unsigned i;
	if (adc < 1170) {
		return 3000;
	} else {
		if (adc > 3366) {
			return 500;
		} else {
			for(i = 0; i < 5; i = i + 1) {
				if(ADCvCM[i] > adc && ADCvCM[i+1] < adc) {
					return ((5*i) - (5 * ((float)adc - ADCvCM[i+1])/(ADCvCM[i]-ADCvCM[i+1]))) * 100;
				}
			}
		}
	}
	return 0;
}

//Control system function
void PControl(void) {
	uint16_t current;
	int error;
	//Convert to distance (lookup table)
	current = X(ADC_VAL);//Distance read
	//Find error with 2 decimals
	error = (current-SETPOINT);
	//Turn into percentage of valve opening
	//output = error * CONTROL_GAIN;//Make the error a percentage of valve PWM range
	if (error > 0) {
		if(error > 500) {//Close saturation
			PWMAngleSet(((float)FULL_CLOSE-MID_CONTROL) * 3/4 + MID_CONTROL);
		} else {
			PWMAngleSet(MID_CONTROL + (((float)FULL_CLOSE - MID_CONTROL) * 3/4 * ((float) error) / 500));
		}
	} else {
		if (error < -500) {//Open saturation
			PWMAngleSet(((float)MID_CONTROL-FULL_OPEN) * 3/4 + FULL_OPEN);
		} else {
				PWMAngleSet(MID_CONTROL + (((float)MID_CONTROL - FULL_OPEN) * 1/4 * ((float) error) / 500));
		}
	}
	ADC_FLAG = 0;
}
