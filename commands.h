#ifndef COMMANDS_H
#define COMMANDS_H

#include <stdint.h>
#include <cstdlib>
#include "registers.h"
#include "UART.h"
#include "timer.h"
#include "ADC.h"
#include "control.h"

//Limit the command length and define length of command list
#define COMMAND_LENGTH 20
#define COMMAND_LIST_LENGTH 11

//Set up command memory
#define COMMAND_BUFFER_LENGTH 10

void LED_ON (char * LED);
void LED_OFF (char * LED);
void HELP (char * data);
void QUERY_LED (char * LED);
void INFO (char * data);
void SETANGLE (char * data);
void VALVEOFF (char * data);
void VALVEON (char * data);
void ADCREAD (char * data);
void ERROR (char * data);
void CONTROL_ON (char * data);
void CONTROL_OFF (char * data);

//Helper functions
int StringToInt (char * data);

#endif
