/////////////////////////////////////
//Copyright 2016 Cameron Gaveronski
//November 18, 2016
//University of Regina
//Electronic Systems Engineering
/////////////////////////////////////

////////////////////////////////////
//    This file is part of SeaBiscuit.
//
//    SeaBiscuit is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    SeaBiscuit is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with SeaBiscuit.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////

#include "SeaBiscuit.h"

struct command_struct * config;//Contains configuration data
char * sea_command;//Holds command characters
char ** sea_command_memory;//Holds past commands
unsigned sea_command_count = 0;
unsigned sea_char_count = 0;//Counts characters in current command
uint8_t sea_init = 0;//Checked to make sure SeaBiscuit is initialized

void sea_history_cmd(char * data);
void sea_bang_function(char * data);
void sea_error_cmd(char * data);
void sea_send_int(int data);

//Loop through character pointer as though it were an array
void sea_send_line(char * data) {
	unsigned i;
	for(i = 0; data[i] != 0; i += 1) {
		config->sea_echo_cmd(data[i]);
	}
}

//Initialize the variables allocated above
int InitBiscuit(struct command_struct * config_in) {
	unsigned i;
	sea_char_count = 0;
	
	config = config_in;
	sea_command = malloc(config->sea_command_length * sizeof(char));
	if(sea_command == 0) {
		sea_send_line("Error: out of memory\r\n");
		return -1;
	}
	sea_command_memory = malloc(config->sea_command_history_size * sizeof(char *));
	if(sea_command_memory == 0) {
		sea_send_line("Error: out of memory\r\n");
		return -1;
	}
	for(i = 0; i < config->sea_command_history_size; i += 1) {
		sea_command_memory[i] = malloc(config->sea_command_length * sizeof(char));
		if(sea_command_memory[i] == 0) {
			sea_send_line("Error: out of memory\r\n");
			return -1;
		}
	}
	
	sea_send_line(" \r\n");
	sea_send_line(config->sea_name);//Command prompt
	sea_send_line(" > ");
	sea_init = 1;
	return 0;//Success
}

void ParseByte(uint8_t commandByte) {
	unsigned k = 0;
	if((commandByte == 0x8) || (commandByte == 0x7F)) {//Check for backspace
		if (sea_char_count > 0) {
			config->sea_echo_cmd(0x8);//Mirror the backspace
			config->sea_echo_cmd(0x20);//Send a whitespace to clear a character
			config->sea_echo_cmd(0x8);//Move back one spot
			
			//Remove the last byte in the command buffer and decrement count
			sea_command[sea_char_count] = 0;
			sea_char_count -= 1;
		}
	} else {
		if (commandByte == 0xD) {//Check for enter key
			config->sea_echo_cmd(0xA);//Send carriage return and line feed to the typewriter
			config->sea_echo_cmd(0xD);
			
			if(sea_char_count != 0) {
				SendCommand(sea_command, 1);
				//Reset command string
				sea_char_count = 0;
				for (k = 0; k < config->sea_command_length; k += 1) {
					sea_command[k] = 0;
				}
			}
			sea_send_line(config->sea_name);//Command prompt
			sea_send_line(" > ");
		} else {
			if ((sea_char_count < (config->sea_command_length - 1)) && (commandByte >= 0x20)) {
				//Add the character to the command and increment counter, ignoring garbage characters
				//Also, capitalize any lower-case characters
				if ((commandByte >= 'a') && (commandByte <= 'z')) {
					commandByte -= 32;
				}
				sea_command[sea_char_count] = commandByte;
				sea_char_count += 1;
				//Mirror the byte
				config->sea_echo_cmd(commandByte);
			}
		}
	}
}

void SendCommand (char * command, uint8_t addBuf) {
	//Execute command
	char * data;
	int c, d, i, j, k, l;
	int dataNum = 0;
	data = malloc(config->sea_command_length * sizeof(char));
	if(data == 0) {
		sea_send_line("Error: out of memory\r\n");
		return;
	}
	
	//First, take out any pairs of whitespaces
	for (c = 0; c < (config->sea_command_length - 1) && command[c] != 0; c += 1) {
		if (command[c] == ' ' && command[c+1] == ' ') {//If 2 whitespaces...
			for (d = c; d < (config->sea_command_length - 1) && command[d] != 0; d += 1) {
				command[d] = command[d+1];//...shift everything after the first space left by one
			}
			c -= 1;//Checks for more than 2 whitespaces
		}
	}
	
	for (i = 0; i < config->sea_command_num; i += 1) {
		for (j = 0; (j < config->sea_command_length) && ((config->sea_command_list[i][j] == command[j]) || (config->sea_command_list[i][j] == 0x0)); j += 1) {
			//Look for complete match, get any data
			if (config->sea_command_list[i][j] == 0) {
				for(dataNum = 0; dataNum < config->sea_command_length - j; dataNum += 1)
					data[dataNum] = command[j + dataNum];
				break;
			}
		}
		//Everything else is data
		if (j + dataNum == config->sea_command_length) {
			//Command found, execute command and add to command history...
			config->sea_function_calls[i](data);
			//...but only if not the "!" command
			if(addBuf && (config->sea_function_calls[i] != sea_bang_function) && (config->sea_function_calls[i] != sea_history_cmd)) {
				//If the command isn't "!" or "HISTORY", and the addBuf flag is true, add the command to the history
				for(k = config->sea_command_history_size - 1; k > 0; k -= 1) {
					//Shift commands up in the buffer
					for (l = 0; l < config->sea_command_length; l += 1) {
						sea_command_memory[k][l] = sea_command_memory[k-1][l];
					}
				}
				//Copy command to lowest location in buffer
				for (k = 0; k < config->sea_command_length; k += 1) {
					sea_command_memory[0][k] = command[k];
				}
			}
			break;
		}
	}
	
	//If nothing found, print an error
	if (i == config->sea_command_num) {
		sea_error_cmd(command);
	}
	free(data);
}

int StringToInt (char * data) {
	int i = 0;//Count variable
	unsigned accumulator = 0;
	if (data[0] == 0) {//Empty string
		return -1;
	}
	
	while (data[i] != 0) {
		if(data[i] < '0' || data[i] > '9') {
			return -1;//Bad character
		} else {
			//Accumulate the value
			accumulator = accumulator * 10 + data[i] - 48;
		}
		i += 1;
	}
	return accumulator;
}

void sea_error_cmd(char * data) {
	sea_send_line("Error: Invalid input (");
	sea_send_line(data);
	sea_send_line(")\r\n");
}

void sea_history_cmd(char * data) {
	int i;
	for(i = config->sea_command_history_size-1; i >= 0; i -= 1) {
		if(sea_command_memory[i][0] != 0) {
			sea_send_int(i + 48);
			sea_send_line(": ");
			sea_send_line(sea_command_memory[i]);
			sea_send_line("\r\n");
		}
	}
}

void sea_bang_function(char * data) {
	if(data[0] == 0) {//Repeat most recent command
		SendCommand(sea_command_memory[0], 0);
	} else {
		SendCommand(sea_command_memory[StringToInt(data)], 0);
	}
}

void sea_send_int(int num) {
	if (num < 0) {//Send negative sign if negative, then make positive
		config->sea_echo_cmd('-');
		num = -num;
	}
	if (num > 9) {
		sea_send_int(num/10);
	}
	//Send ASCII character for given number
	config->sea_echo_cmd((num % 10) + 48);
}
