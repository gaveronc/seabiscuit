/////////////////////////////////////
//Copyright 2016 Cameron Gaveronski
//November 18, 2016
//University of Regina
//Electronic Systems Engineering
/////////////////////////////////////

////////////////////////////////////
//    This file is part of SeaBiscuit.
//
//    SeaBiscuit is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    SeaBiscuit is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with SeaBiscuit.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////

#ifndef __SEA_BISCUIT_H__
#define __SEA_BISCUIT_H__

#include <stdint.h>
#include <stdlib.h>

struct command_struct {
	char * sea_name;
	char ** sea_command_list;
	void (**sea_function_calls)(char * data);
	unsigned sea_command_length;
	unsigned sea_command_num;
	unsigned sea_command_history_size;
	void (*sea_echo_cmd)(uint8_t data);
	
};

void ParseByte(uint8_t commandByte);
void SendCommand(char * command, uint8_t addBuf);
int InitBiscuit(struct command_struct * config_in);
void sea_bang_function(char * data);
void sea_history_cmd(char * data);

#endif
