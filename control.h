#ifndef CONTROL_H
#define CONTROL_H
#include <stdint.h>
#include "commands.h"

#define FULL_CLOSE 4500		//Percentage to fully close valve
#define FULL_OPEN 0				//Percentage to fully open valve
#define MID_CONTROL 3000	//A mid-point to work with for testing purposes

#define M -87.84	//Slope
#define B 3366									//Y-intercept

extern uint8_t ADC_FLAG;//Conversion complete
extern uint16_t ADC_VAL;//Communication with main
extern uint16_t SETPOINT;//Valve set point, currently not used
extern uint16_t CONTROL_GAIN;//Control loop proportional gain

void PControl(void);

#endif
